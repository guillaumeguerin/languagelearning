var Datastore = require('nedb');
var message = require('../beans/message.js')

exports.loadLessonDatabase = function() {
	var lessonsDb = 'database/lessons.db'
	db = new Datastore({ filename: lessonsDb, autoload: true })
	return db;
};

exports.findAllLessons = function(db, event, args, myUserName, myLanguage) {
	db.find({user : myUserName, name: myLanguage}, function (err, docs) {
		docs.forEach(function(element) {
	    		event.sender.send('lessonFound', "lessonFound:" + element.name + ":" + element.title + ":" +  element.user + ":" + element.status + ":" + element.rating);
		});
	});
};

exports.findLessonWithName = function(db, n) {
	db.find({ name: n}, function (err, docs) {
	  return docs;
	});
};

exports.saveLesson = function(db, languageName, lessonTitle, userName, rating , isDone) {
	// Using a sparse unique index
	db.ensureIndex({ fieldName: 'title', unique: true, sparse: true }, function (err) {
	});
	// Format of the error message when the unique constraint is not met
	db.insert({name: languageName, title: lessonTitle, user: userName, status:isDone, rating: rating}, function (err) {
	});
};
