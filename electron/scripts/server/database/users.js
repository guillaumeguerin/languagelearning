var Datastore = require('nedb');
var message = require('../beans/message.js')

exports.loadUserDatabase = function() {
	var usersDb = 'database/users.db'
	db = new Datastore({ filename: usersDb, autoload: true })
	return db;
};

exports.findAllUsers = function(db, event, args) {
	db.find({}, function (err, docs) {
		docs.forEach(function(element) {
	    		event.sender.send('userFound', "userFound:" + element.name);
		});
	});
};

exports.findUserWithName = function(db, n) {
	db.find({ name: n}, function (err, docs) {
	  return docs;
	});
};

exports.saveUser = function(db, userName, userPassword, userEmail) {
	// Using a sparse unique index
	db.ensureIndex({ fieldName: 'name', unique: true, sparse: true }, function (err) {
	});
	// Format of the error message when the unique constraint is not met
	db.insert({name: userName, password: userPassword, email: userEmail }, function (err) {
	});
};

exports.updateUser = function(db, userName, userEmail) {
	db.update({ name: username }, { $set: { email: userEmail } }, { multi: true }, function (err, numReplaced) {
	});
};

exports.deleteUser = function(db, userName) {
	db.remove({ name: userName }, { multi: true }, function (err, numRemoved) {
	});
};
