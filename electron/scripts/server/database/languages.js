var Datastore = require('nedb');
var message = require('../beans/message.js')

exports.loadUserDatabase = function() {
	var languagesDb = 'database/languages.db'
	db = new Datastore({ filename: languagesDb, autoload: true })
	return db;
};

exports.findAllLanguages = function(db, event, args) {
	db.find({}, function (err, docs) {
		docs.forEach(function(element) {
	    		event.sender.send('languageFound', "languageFound:" + element.name + ":" + element.icon + ":" +  element.nbLesson);
		});
	});
};

exports.findLanguageWithName = function(db, n) {
	db.find({ name: n}, function (err, docs) {
	  return docs;
	});
};

exports.saveLanguage = function(db, languageName, languageIcon, languageFolder, nbOfLessons) {
	// Using a sparse unique index
	db.ensureIndex({ fieldName: 'name', unique: true, sparse: true }, function (err) {
	});
	// Format of the error message when the unique constraint is not met
	db.insert({name: languageName, icon: languageIcon, folder: languageFolder, nbLesson: nbOfLessons}, function (err) {
	});
};
