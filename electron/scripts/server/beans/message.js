const {ipcMain} = require('electron');

exports.asyncToClient = function(text) {
	// Event handler for asynchronous incoming messages
	ipcMain.on('asynchronous-message', (event, arg) => {
	   //console.log(arg)

    // Event emitter for sending asynchronous messages
    event.sender.send('asynchronous-reply', text)
	})
}

exports.asyncToClientLanguage = function(text) {
    ipcMain.send('languageFound', text)
}

exports.asyncToClientUser = function(text) {
	// Event handler for asynchronous incoming messages
	ipcMain.on('user-message', (event, arg) => {
	   //console.log(arg)

    // Event emitter for sending asynchronous messages
    event.sender.send('userFound', text)
	})
}
