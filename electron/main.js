const {app, BrowserWindow} = require('electron')
const {ipcMain} = require('electron')
const {shell} = require('electron')
const url = require('url')
const path = require('path')
const fs = require('fs')
const http = require('http')
const Datastore = require('nedb')
const unzip = require('unzip')
const yauzl = require("yauzl")
const yazl = require("yazl")
const extract = require('extract-zip')
const { lstatSync, readdirSync } = require('fs')
const { join } = require('path')

const model = require('./lib/model.js')
const userService = require('./scripts/server/database/users.js')
const languageService = require('./scripts/server/database/languages.js')
const lessonService = require('./scripts/server/database/lessons.js')
var win

var currentUser;
var currentLanguage;
var currentLesson;
var currentLessonTitle;

function createWindow() {
   win = new BrowserWindow({width: 800, height: 600,icon: __dirname + '/world.ico'})
   win.loadURL(url.format({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file:',
      slashes: true
   }))
}

/*var file = fs.createWriteStream("peace.zip");
var request = http.get("http://www.colorado.edu/conflict/peace/download/peace.zip", function(response) {
  response.pipe(file);*/
/*
var zipfile = new yazl.ZipFile();
zipfile.addFile("file1.txt", "file1.txt");
zipfile.outputStream.pipe(fs.createWriteStream("lessonSample.zip")).on("close", function() {
  console.log("done");
});
zipfile.end();*/

var catsZip = path.join(__dirname, 'lessonSample.zip')
var absoluteTarget = path.join(__dirname, 'lessonSamplezzz')
extract(catsZip, {dir: absoluteTarget}, function (err) {
	// extraction is complete. make sure to handle the err
	console.log(err);
});

let onlineStatusWindow

app.on('ready', () => {
  onlineStatusWindow = new BrowserWindow({ width: 0, height: 0, show: false })
  onlineStatusWindow.loadURL(`file://${__dirname}/online-status.html`)
})

ipcMain.on('online-status-changed', (event, status) => {
  console.log(status)
})


// Event handler for asynchronous incoming messages
ipcMain.once('asynchronous-message', (event, arg) => {
   checkAsyncCall(event, arg);
})

ipcMain.on('listUsers', (event, arg) => {
  console.log(arg);
  var db = userService.loadUserDatabase();
  var docs = userService.findAllUsers(db, event, arg);
  console.log(docs);
})

ipcMain.on('selectedUser', (event, arg) => {
	if(arg.split(":")[0] == 'selectedUser') {
		currentUser = arg.split(":")[1];
	}
})

ipcMain.on('languageFound', (event, arg) => {
   checkAsyncCall(event, arg);
})

ipcMain.on('listLessons', (event, arg) => {
   event.sender.send('listLessons', arg + '' + getDirectories(__dirname + "/lessons/" + arg).length);
   //checkAsyncCall(event, arg);
})

ipcMain.on('whichUser', (event, arg) => {
   if(arg.split(":")[0] == 'whichUser') {
		event.sender.send('whichUser', 'whichUser:' + currentUser);
	}
})

ipcMain.on('readTOS', (event, arg) => {
	shell.openExternal('https://github.com');
})

ipcMain.on('createUser', (event, arg) => {
   if(arg.split(":")[0] == 'createUser') {
		userName = arg.split(":")[1];
		userPassword = arg.split(":")[2];
		userEmail = arg.split(":")[3];
		var fileStr = model.serialize({name: userName, password: userPassword, email: userEmail }) + '\n';
		var usersDb = 'database/users.db'
		var db = new Datastore({ filename: usersDb, autoload: true })
		// Using a sparse unique index
		db.ensureIndex({ fieldName: 'name', unique: true, sparse: true }, function (err) {
		});
		// Format of the error message when the unique constraint is not met
		db.insert({name: userName, password: userPassword, email: userEmail }, function (err) {
		});
	}
})

ipcMain.on('whichLanguage', (event, arg) => {
  console.log(arg);
  var db = languageService.loadUserDatabase();
  languageService.findAllLanguages(db, event, arg);
})

//lessons.html
ipcMain.on('languageSelected', (event, arg) => {
  currentLanguage = arg;
})

ipcMain.on('whichSelectedLanguage', (event, arg) => {
	event.sender.send('languageSelected', currentLanguage);
})

ipcMain.on('sendAllAvalaibleLessons', (event, arg) => {
	var db = lessonService.loadLessonDatabase();
	lessonService.findAllLessons(db, event, arg, currentUser, currentLanguage);
})

ipcMain.on('selectedLesson', (event, arg) => {
	console.log(arg);
	currentLesson = arg.split(":")[1];
	currentLessonTitle = arg.split(":")[2];
})

//singlelesson.html
ipcMain.on('whichSelectedLesson', (event, arg) => {
	var data = fs.readFileSync('lessons/' + currentLanguage.toLowerCase() + '/lesson' + currentLesson + '/lesson.json');
	event.sender.send('lessonContent', data.toString());
})

ipcMain.on('selectedLessonIsDone', (event, arg) => {
	var db = lessonService.loadLessonDatabase();
	db.update({ title: currentLessonTitle }, { $set: { status: '✔️' } }, {}, function (err, numReplaced) {
		console.log(err);
	});
})

ipcMain.on('whichSelectedLessonId', (event, arg) => {
	event.sender.send('lessonId', currentLesson);
})

ipcMain.on('resetLesson', (event, arg) => {
	var db = lessonService.loadLessonDatabase();
	db.update({ title: currentLessonTitle }, { $set: { status: 'To do' } }, {}, function (err, numReplaced) {
		console.log(err);
	});
})

function asyncToClient(text) {
	// Event handler for asynchronous incoming messages
	ipcMain.on('asynchronous-message', (event, arg) => {
	   //console.log(arg)

    // Event emitter for sending asynchronous messages
    event.sender.send('asynchronous-reply', text)
	})
}
function checkAsyncCall(event, arg) {
  console.log(arg);
}

/*function initLanguagesDatabase() {
	var dbLanguage = languageService.loadUserDatabase();
	languageService.saveLanguage(dbLanguage, 'French', 'france.svg', 'french', 1);
	languageService.saveLanguage(dbLanguage, 'Chinese (Mandarin)', 'china.svg', 'mandarin', 1);
	languageService.saveLanguage(dbLanguage, 'Spanish', 'spain.svg', 'spanish', 1);
}*/

//initLanguagesDatabase();

function initLessonsDatabase() {
	var dbLesson = lessonService.loadLessonDatabase();
	lessonService.saveLesson(dbLesson, 'French', 'Leçon 1', 'Guillaume', 1, "To do");
	lessonService.saveLesson(dbLesson, 'French', 'Leçon 2', 'Guillaume', 2, "To do");
	lessonService.saveLesson(dbLesson, 'Chinese (Mandarin)', '课 1', 'Guillaume', 1, "To do");
	lessonService.saveLesson(dbLesson, 'Spanish', 'Lección 1', 'Guillaume', 1, "To do");
}

initLessonsDatabase();

function getDirectories(path) {
  return fs.readdirSync(path).filter(function (file) {
    return fs.statSync(path+'/'+file).isDirectory();
  });
}


console.log(getDirectories(__dirname + "/lessons/french").length);
 
app.on('ready', createWindow)
